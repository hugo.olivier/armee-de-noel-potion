class ExisteDeja(Exception):
    pass

class Ingredient:
    dic = {}
    liquides = {}
    def __init__(self,
                nom,
                diminutif=None,
                liquide=False,
                regle_speciale=lambda *_,**__: True,
                doses=1,
                description="un ingrédient"):
        self.nom = nom
        self.diminutif = diminutif or nom
        self.liquide = liquide
        self.regle_speciale = regle_speciale
        self.doses = doses
        self.eau = False
        self.melange = False
        self.description = description

        if Ingredient.dic.get(self.diminutif,False):
            raise ExisteDeja()
        
        Ingredient.dic[self.diminutif] = self
        if self.liquide:
            Ingredient.liquides[self.diminutif] = self

    def __repr__(self):
        return self.description

Ingredient("eau",'e',True,doses=0,description="L'eau, l'ingrédient de base.").eau = True
Ingredient("melange",'m',False,doses=0,description="L'action de mélanger, pas vraiment un ingrédient mais une étape capitale.").melange =True

def compte_doses(l,i):
    c = 0
    for k in range(i+1):
        c+=Ingredient.dic[l[k]].doses
    return c

def regle1(l,i):
    if Ingredient.dic[l[i]].doses>0 and compte_doses(l, i)%3==0:
        r=1
        while i+r<len(l):
            if Ingredient.dic[l[i+r]].eau:
                return True
            if Ingredient.dic[l[i+r]].doses>0:
                return False
            r+=1
        return False
    return True

def regle2(l,i):
    if Ingredient.dic[l[i]].doses>0:
        r=1
        while i-r>=0:
            if l[i-r]==l[i]:
                return False
            if Ingredient.dic[l[i-r]].doses>0 or Ingredient.dic[l[i-r]].eau:
                return True
            r+=1
        return True
    return True

def regle3(l,i):
    if Ingredient.dic[l[i]].liquide:
        return (i+1<len(l) and Ingredient.dic[l[i+1]].melange)
    if Ingredient.dic[l[i]].melange:
        return (i>0 and Ingredient.dic[l[i-1]].liquide)
    return True

def regle4(l,i):
    if Ingredient.dic[l[i]].eau:
        return (i-2<0 or not Ingredient.dic[l[i-2]].eau)
    return True

def verif_sciure(l,i):
    n = 2 if compte_doses(l, i)%3==0 else 1
    r=1
    while i+r<len(l):
        if Ingredient.dic[l[i+r]].eau:
            n-=1
        if Ingredient.dic[l[i+r]].doses>0:
            return False
        if n==0:
            return True
        r+=1
    return False

Ingredient("jus de citron",'jdc',True,lambda l,i: i+2>=len(l) or not Ingredient.dic[l[i+2]].eau, description = "Du jus de citron, il ne faut pas mettre de l'eau juste après.")
Ingredient("farine",'f',description = "De la farine.")
Ingredient("sciure de bois",'sc',regle_speciale=verif_sciure, description = "De la sciure de bois, il faut mettre de l'eau juste après.")
Ingredient("baie de genievre",'b',regle_speciale = lambda l,i: sum(Ingredient.dic[l[k]].doses for k in range(i,len(l)))<=3, description = "Une baie de genièvre, doit être ajoutée vers la fin.")
Ingredient("feuille de blette",'fdb',description = "Une feuille de blette, tout simplement.")
Ingredient("poils de belette",'pdb',regle_speciale=lambda l,i: 'f' in l[:i], description = "Des poils de belette, à ne pas confondre avec la feuille de blette, il faut de la farine dans la potion pour en mettre.")
Ingredient("bave de limace",'bdl',True,regle_speciale=lambda l,i: i==0 or not Ingredient.dic[l[i-1]].melange,description = "De la bave de limace, dégueu, il ne faut pas mélanger avant.")
Ingredient("houx",'h',regle_speciale = lambda l,i: i>0 and Ingredient.dic[l[i-1]].melange, description = "Du houx, il faut avoir mélangé avant.")
Ingredient("chapon",'c',regle_speciale = lambda l,i: compte_doses(l, i)%3!=1,doses=2,description="Du chapon, ça compte pour 2 doses d'ingrédients (et on ne peut pas mettre de l'eau entre les deux).")
Ingredient("marrons",'mr',regle_speciale = lambda l,i: i<=1 or not Ingredient.dic[l[i-2]].eau, description = "Des marrons chauds, ils ne doivent pas être mis juste après de l'eau.")

def verification_totale(l,recette):
    err = []
    print("Rappel de la préparation donnée :")
    nb_regle4 = 0
    for i in range(len(l)):
        ing = Ingredient.dic[l[i]]
        print(f"[{i}] {ing.nom}")
        if not regle1(l, i):
            err.append(f"[{i}] Règle #1 : il fallait mettre de l'eau après l'ingrédient [{ing.nom}] car c'est le 3e")
        if not regle2(l,i):
            err.append(f"[{i}] Règle #2 : il ne fallait pas mettre cet ingrédient [{ing.nom}] deux fois d'affilée")
        if not regle3(l,i):
            if ing.liquide:
                err.append(f"[{i}] Règle #3 : l'ingrédient [{ing.nom}] est un liquide, il fallait mélanger juste après.")
            if ing.melange:
                err.append(f"[{i}] Règle #3 : il ne fallait pas mélanger car l'ingrédient précédent n'est pas un liquide")
        if not regle4(l,i):
            nb_regle4 += 1
        if not ing.regle_speciale(l,i):
            err.append(f"[{i}] Règle de [{ing.nom}]. La description donne : '{ing.description}'.")
    ingredients_retires = []
    if nb_regle4>0:
        print("\nIngredients retirés par la regle 4 :")
        for i in range(len(l)):
            if (ing := Ingredient.dic[l[i]]).doses>0:
                print(f"[{i}] {ing.nom}")
                ingredients_retires.append(l[i])
                nb_regle4 -=1
            if nb_regle4 == 0:
                break
    print("\nRésultat :")
    for e in err:
        print(e)
    if len(err)==0:
        print("La potion est valide\n")
    else :
        print("\nla potion n'est pas valide\n")
    if verification_recette(l, recette, ingredients_retires):
        print("\nLa potion respecte la recette")
    else:
        print("\nla potion ne respecte pas la recette")

def verification_recette(l,recette, retires):
    valide = True
    for i,n in recette:
        if i=='f':
            n=l.count('e')
        if (s:=(l.count(i) - retires.count(i))) != n:
            print(f"[{Ingredient.dic[i].nom}] donné : {s}, nécessaire : {n}")
            valide = False
    return valide

recette = [('jdc',2),('f',0),('sc',2),('b',1),('fdb',1),('pdb',1),('bdl',1),('h',2),('c',1),('mr',2)]

def parse_recette():
    print("Saisissez la recette, ingrédient par ingrédient (en utilisant les diminutifs)")
    l = []
    while True:
        txt = input()
        if txt:
            l.append(txt)
        else:
            break
    return l

if __name__=="__main__":
    l = parse_recette()
    verification_totale(l, recette)
